﻿using System;
using System.Text;
using Lab01_MatrixOperations.Library;

namespace Lab03_DesktopApplication.WpfApp
{
    internal static class MatrixManipulator
    {
        internal static string ConvertMatrixToFormattedString(MathMatrix matrix)
        {
            StringBuilder matrixStringBuilder = new StringBuilder();

            for (int i = 0; i < matrix.Rows; i++)
            {
                for (int j = 0; j < matrix.Columns; j++)
                {
                    matrixStringBuilder.Append(string.Format("{0:0.000}\t", matrix[i, j]));
                }
                matrixStringBuilder.AppendLine();
            }
            matrixStringBuilder.AppendLine();

            return matrixStringBuilder.ToString();
        }
    }
}