﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mime;
using Lab01_MatrixOperations.Library;
using Microsoft.Win32;

namespace Lab03_DesktopApplication.WpfApp
{
    internal static class FileOperator
    {
        internal static MathMatrix ReadMatrixFromFile()
        {
            MathMatrix readMatrix = null;
            List<string> lines = new List<string>();
            OpenFileDialog openFileDialog = new OpenFileDialog();

            try
            {
                if (openFileDialog.ShowDialog() == true)
                    using (StreamReader streamReader = new StreamReader(openFileDialog.FileName))
                        while (!streamReader.EndOfStream)
                            lines.Add(streamReader.ReadLine());
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error with opening file:\n{0}", ex.Message);
                MessageBoxManager.ShowDetailedErrorMessageBox("Error with opening file", ex);
            }

            for (int i = 0; i < lines.Count; i++)
            {
                string[] splitedNumbers = lines[i].Split(null);
                if (i == 0)
                {
                    if (splitedNumbers.Length != 2)
                    {
                        throw new ArgumentException("Given matrix has wrong parameters");
                    }

                    int rows = 0;
                    int.TryParse(splitedNumbers[0], out rows);
                    int columns = 0;
                    int.TryParse(splitedNumbers[1], out columns);

                    readMatrix = new MathMatrix(rows, columns);
                }
                else
                {
                    for (int j = 0; j < splitedNumbers.Length; j++)
                    {
                        double numberFromFile = 0;
                        double.TryParse(splitedNumbers[j], out numberFromFile);
                        readMatrix[i - 1, j] = numberFromFile;
                    }
                }
            }

            return readMatrix;
        }

        internal static void WriteMatrixToFile(MathMatrix mathMatrix)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                CreatePrompt = true,
                OverwritePrompt = true,
                DefaultExt = "txt",
                Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*"
            };

            try
            {
                if (saveFileDialog.ShowDialog() != true) return;

                using (StreamWriter streamWriter = new StreamWriter(saveFileDialog.FileName))
                {
                    streamWriter.Write("{0} {1}", mathMatrix.Rows, mathMatrix.Columns);
                    streamWriter.WriteLine();

                    for (int i = 0; i < mathMatrix.Rows; i++)
                    {
                        for (int j = 0; j < mathMatrix.Columns; j++)
                        {
                            streamWriter.Write("{0:0.000}", mathMatrix[i, j]);
                            if (j != mathMatrix.Columns - 1)
                                streamWriter.Write("\t");
                        }
                        streamWriter.WriteLine();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error with writing file:\n{0}", ex.Message);
                MessageBoxManager.ShowDetailedErrorMessageBox("Error with opening file", ex);
            }
        }
    }
}