﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using Lab01_MatrixOperations.Library;
using System.Windows;
using Lab03_DesktopApplication.WpfApp.Enums;

namespace Lab03_DesktopApplication.WpfApp
{
    internal class MainWindowViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        #region Commands

        public ICommand LoadFirstMatrix { get; set; }
        public ICommand LoadSecondMatrix { get; set; }
        public ICommand SaveResultMatrix { get; set; }
        public ICommand ExitApplication { get; set; }
        public ICommand DisplayAboutInformation { get; set; }
        public ICommand Calculate { get; set; }

        #endregion

        #region Properties

        public bool SaveResultButtonIsEnabled
        {
            get { return _saveResultButtonIsEnabled; }
            set
            {
                _saveResultButtonIsEnabled = value;
                OnPropertyChanged("SaveResultButtonIsEnabled");
            }
        }

        public bool CalculateButtonIsEnabled
        {
            get { return _calculateButtonIsEnabled; }
            set
            {
                _calculateButtonIsEnabled = value;
                OnPropertyChanged("CalculateButtonIsEnabled");
            }
        }

        public string FirstMatrixTextBoxText
        {
            get { return _firstMatrixTextBoxText; }
            set
            {
                _firstMatrixTextBoxText = value;
                OnPropertyChanged("FirstMatrixTextBoxText");
            }
        }

        public string SecondMatrixTextBoxText
        {
            get { return _secondMatrixTextBoxText; }
            set
            {
                _secondMatrixTextBoxText = value;
                OnPropertyChanged("SecondMatrixTextBoxText");
            }
        }

        public string ThirdMatrixTextBoxText
        {
            get { return _thirdMatrixTextBoxText; }
            set
            {
                _thirdMatrixTextBoxText = value;
                OnPropertyChanged("ThirdMatrixTextBoxText");
            }
        }

        public MatrixOperations SelectedOperation
        {
            get { return _selectedOperation; }
            set
            {
                _selectedOperation = value;
                OnPropertyChanged("SelectedOperation");
            }
        }

        #endregion

        #region Constructors

        public MainWindowViewModel()
        {
            LoadFirstMatrix = new RelayCommand(LoadFirstMatrixExecute, CanLoadFirstMatrixExecute);
            LoadSecondMatrix = new RelayCommand(LoadSecondMatrixExecute, CanLoadSecondMatrixExecute);
            SaveResultMatrix = new RelayCommand(SaveResultMatrixExecute, CanSaveResultMatrixExecute);
            ExitApplication = new RelayCommand(ExitApplicationExecute, CanExitApplicationExecute);
            DisplayAboutInformation = new RelayCommand(DisplayAboutInformationExecute, CanDisplayAboutInformationExecute);
            Calculate = new RelayCommand(CalculateExecute, CanCalculateExecute);

            _backgroundWorker = new BackgroundWorker();
            _backgroundWorker.DoWork += MatrixCalculation;

            SaveResultButtonIsEnabled = false;
            CalculateButtonIsEnabled = false;
            SelectedOperation = MatrixOperations.Add;
            }

        #endregion

        #region Fields

        private MathMatrix _firstMatrix;
        private MathMatrix _secondMatrix;
        private MathMatrix _thirdMatrix;
        private BackgroundWorker _backgroundWorker;
        private bool _saveResultButtonIsEnabled;
        private bool _calculateButtonIsEnabled;
        private string _firstMatrixTextBoxText;
        private string _secondMatrixTextBoxText;
        private string _thirdMatrixTextBoxText;
        private MatrixOperations _selectedOperation;

        #endregion

        private void MatrixCalculation(object sender, DoWorkEventArgs e)
        {
            try
            {
                SaveResultButtonIsEnabled = false;
                CalculateButtonIsEnabled = false;

                switch (SelectedOperation)
                {
                    case MatrixOperations.Add:
                        _thirdMatrix = _firstMatrix + _secondMatrix;
                        break;
                    case MatrixOperations.Substract:
                        _thirdMatrix = _firstMatrix - _secondMatrix;
                        break;
                    case MatrixOperations.Multiple:
                        _thirdMatrix = _firstMatrix*_secondMatrix;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                ThirdMatrixTextBoxText = MatrixManipulator.ConvertMatrixToFormattedString(_thirdMatrix);
            }
            catch (Exception ex)
            {
                MessageBoxManager.ShowDetailedErrorMessageBox("Error with matrix calculations", ex);
            }
            finally
            {
                RefreshButtons();
            }
        }

        private void RefreshButtons()
        {
            if (_firstMatrix != null && _secondMatrix != null)
                CalculateButtonIsEnabled = true;
            else
                CalculateButtonIsEnabled = false;

            SaveResultButtonIsEnabled = _thirdMatrix != null;
        }

        #region CommandExecutions

        private void CalculateExecute()
        {
            _backgroundWorker.RunWorkerAsync();
        }

        private void DisplayAboutInformationExecute()
        {
            MessageBoxManager.ShowAboutMessageBox();
        }

        private void ExitApplicationExecute()
        {
            Application.Current.Shutdown();
        }

        private void SaveResultMatrixExecute()
        {
            FileOperator.WriteMatrixToFile(_thirdMatrix);
        }

        private void LoadFirstMatrixExecute()
        {
            _firstMatrix = FileOperator.ReadMatrixFromFile();
            FirstMatrixTextBoxText = MatrixManipulator.ConvertMatrixToFormattedString(_firstMatrix);
            RefreshButtons();
        }

        private void LoadSecondMatrixExecute()
        {
            _secondMatrix = FileOperator.ReadMatrixFromFile();
            SecondMatrixTextBoxText = MatrixManipulator.ConvertMatrixToFormattedString(_secondMatrix);
            RefreshButtons();
        }

        #endregion

        #region CommandConditions

        private bool CanCalculateExecute()
        {
            return true;
        }

        private bool CanDisplayAboutInformationExecute()
        {
            return true;
        }

        private bool CanExitApplicationExecute()
        {
            return true;
        }

        private bool CanSaveResultMatrixExecute()
        {
            return true;
        }

        private bool CanLoadSecondMatrixExecute()
        {
            return true;
        }

        private bool CanLoadFirstMatrixExecute()
        {
            return true;
        }

        #endregion

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            handler?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
