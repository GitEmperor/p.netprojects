﻿using System.Collections.Generic;

namespace Lab01_MatrixOperations.Library
{
    public interface IMatrix<T> : IEnumerable<T>
    {
        /// <summary>
        /// Gets rows number
        /// </summary>
        int Rows { get; }

        /// <summary>
        /// Gets columns number
        /// </summary>
        int Columns { get; }

        /// <summary>
        /// Iterator accesser
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        T this[int row, int column] { get; set; }

        /// <summary>
        /// Checks if given matrix is equivalent to the present
        /// </summary>
        /// <param name="comparedMatrix"></param>
        /// <returns></returns>
        bool Equals(IMatrix<T> comparedMatrix);
    }
}
