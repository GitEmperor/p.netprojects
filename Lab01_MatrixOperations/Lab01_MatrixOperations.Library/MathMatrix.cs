﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Lab01_MatrixOperations.Library
{
    public class MathMatrix : IMatrix<double>
    {
        private double[,] _arrayOfNumbers;

        #region Constructors
        public MathMatrix(int rows, int columns)
        {
            _arrayOfNumbers = new double[rows, columns];
        }

        public MathMatrix(double[,] initArray)
        {
            _arrayOfNumbers = (double[,])initArray.Clone();
        }
        #endregion

        #region Properties

        public int Rows => _arrayOfNumbers.GetLength(0);

        public int Columns => _arrayOfNumbers.GetLength(1);

        public double this[int row, int column]
        {
            get { return _arrayOfNumbers[row, column]; }
            set { _arrayOfNumbers[row, column] = value; }
        }
        #endregion

        #region IEnumerable<T>
        public IEnumerator<double> GetEnumerator()
        {
            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Columns; j++)  
                    yield return this[i, j];           
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion

        public double SumOfAllElements()
        {
            return this.Sum();
        }

        public double[,] ToArray()
        {
            return (double[,])_arrayOfNumbers.Clone();
        }

        public bool Equals(IMatrix<double> matrix)
        {
            if (Rows != matrix.Rows || Columns != matrix.Columns)
                return false;

            for (int i = 0; i < Rows; i++)
                for (int j = 0; j < Columns; j++)
                    if (Math.Abs(this[i, j] - matrix[i, j]) > double.Epsilon)
                        return false;

            return true;
        }

        #region Operators
        public static MathMatrix operator +(MathMatrix firstMatrix, MathMatrix secondMatrix)
        {
            if (firstMatrix.Rows != secondMatrix.Rows || firstMatrix.Columns != secondMatrix.Columns)
                throw new ArgumentException("Matrices must have the same size");

            MathMatrix resultMatrix = new MathMatrix(firstMatrix.Rows, firstMatrix.Columns);

            for (int i = 0; i < resultMatrix.Rows; i++)
                for (int j = 0; j < resultMatrix.Columns; j++)
                    resultMatrix[i, j] = firstMatrix[i, j] + secondMatrix[i, j];
           
            return resultMatrix;
        }

        public static MathMatrix operator -(MathMatrix firstMatrix, MathMatrix secondMatrix)
        {
            if (firstMatrix.Rows != secondMatrix.Rows || firstMatrix.Columns != secondMatrix.Columns)
                throw new ArgumentException("Matrices must have the same size");

            MathMatrix resultMatrix = new MathMatrix(firstMatrix.Rows, firstMatrix.Columns);

            for (int i = 0; i < resultMatrix.Rows; i++)
                for (int j = 0; j < resultMatrix.Columns; j++)
                    resultMatrix[i, j] = firstMatrix[i, j] - secondMatrix[i, j];

            return resultMatrix;
        }

        public static MathMatrix operator *(MathMatrix firstMatrix, MathMatrix secondMatrix)
        {
            if (firstMatrix.Columns != secondMatrix.Rows)
                throw new ArgumentException("Wrong matrix sizes");

            MathMatrix resultMatrix = new MathMatrix(firstMatrix.Rows, secondMatrix.Columns);

            double tempSum = 0;

            for (int i = 0; i < resultMatrix.Rows; i++)
            {
                for (int j = 0; j < resultMatrix.Columns; j++)
                {
                    for (int k = 0; k < firstMatrix.Columns; k++)
                    {
                        tempSum += firstMatrix[i, k]*secondMatrix[k, j];
                    }
                    resultMatrix[i, j] = tempSum;
                    tempSum = 0;
                }
            }

            return resultMatrix;
        }
        #endregion
    }
}
