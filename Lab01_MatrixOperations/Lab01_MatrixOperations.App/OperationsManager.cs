﻿using System;
using Lab01_MatrixOperations.Library;

namespace Lab01_MatrixOperations.App
{
    internal class OperationsManager
    {
        private MatrixManipulator _matrixManipulator;
        private MathMatrix _matrixA, _matrixB;

        public OperationsManager()
        {
            _matrixManipulator = new MatrixManipulator();
            _matrixA = new MathMatrix(3, 3);
            _matrixB = new MathMatrix(3, 3);

            Random randomGenerator = new Random();

            for (int i = 0; i < _matrixA.Rows; i++)
            {
                for (int j = 0; j < _matrixA.Columns; j++)
                {
                    _matrixA[i, j] = randomGenerator.NextDouble();
                    _matrixB[i, j] = randomGenerator.NextDouble();
                }
            }
        }

        public void StartMatrixOperations()
        {           
            char selection = char.MinValue;            

            while (selection != '6')
            {
                ShowMatricesAndMenu();
                selection = Console.ReadKey().KeyChar;
                Console.WriteLine();

                Console.Clear();
                ManageSelection(selection);
            }                                       
        }

        private void ManageSelection(char selection)
        {
            MathMatrix result;
            MathMatrix newMatrix;

            switch (selection)
            {
                case '1':
                    result = _matrixManipulator.Add(_matrixA, _matrixB);
                    Console.WriteLine("Matrix A:\n");
                    _matrixManipulator.ShowMatrix(_matrixA);
                    Console.WriteLine("Matrix B:\n");
                    _matrixManipulator.ShowMatrix(_matrixB);        
                    if (result != null)
                    {
                        Console.WriteLine("A + B:\n");
                        _matrixManipulator.ShowMatrix(result);
                    }                       
                    break;
                case '2':
                    result = _matrixManipulator.Substract(_matrixA, _matrixB);
                    Console.WriteLine("Matrix A:\n");
                    _matrixManipulator.ShowMatrix(_matrixA);
                    Console.WriteLine("Matrix B:\n");
                    _matrixManipulator.ShowMatrix(_matrixB);
                    if (result != null)
                    {
                        Console.WriteLine("A - B:\n");
                        _matrixManipulator.ShowMatrix(result);
                    }
                    break;
                case '3':
                    result = _matrixManipulator.Multiplicate(_matrixA, _matrixB);
                    Console.WriteLine("Matrix A:\n");
                    _matrixManipulator.ShowMatrix(_matrixA);
                    Console.WriteLine("Matrix B:\n");
                    _matrixManipulator.ShowMatrix(_matrixB);
                    if (result != null)
                    {
                        Console.WriteLine("A * B:\n");
                        _matrixManipulator.ShowMatrix(result);
                    }
                    break;
                case '4':
                    newMatrix = _matrixManipulator.GetNewMatrixFromUser();
                    if (newMatrix != null)
                        _matrixA = newMatrix;
                    break;
                case '5':
                    newMatrix = _matrixManipulator.GetNewMatrixFromUser();
                    if (newMatrix != null)
                        _matrixB = newMatrix;
                    break;
                default:
                    Console.WriteLine("Wrong selection. Press correct number.\n");
                    break;
            }
        }

        private void ShowMatricesAndMenu()
        {
            Console.WriteLine("Matrix A:\n");
            _matrixManipulator.ShowMatrix(_matrixA);
            Console.WriteLine("Matrix B:\n");
            _matrixManipulator.ShowMatrix(_matrixB);

            Console.WriteLine("OPERATIONS MENU:\n" +
                  "1. Add matrices\n" +
                  "2. Substract matrices\n" +
                  "3. Multiplicate matrices\n" +
                  "4. Change matrix A\n" +
                  "5. Change matrix B\n" +
                  "6. Exit\n\n" +
                  "Press selected number\n");
        }
    }
}
