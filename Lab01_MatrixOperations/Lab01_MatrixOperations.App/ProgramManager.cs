﻿using System;

namespace Lab01_MatrixOperations.App
{
    public class ProgramManager
    {
        private OperationsManager _operationsManager;
        private AssemblyManager _assemblyManager;
        private FileManager _fileManager;

        public void StartProgram()
        {
            _operationsManager = new OperationsManager();
            _assemblyManager = new AssemblyManager();
            _fileManager = new FileManager();

            char selection = char.MinValue;

            while (selection != '5')
            {
                ShowMenu();
                selection = Console.ReadKey().KeyChar;
                Console.WriteLine();

                Console.Clear();
                ManageSelection(selection);
            }             
        }

        private void ManageSelection(char selection)
        {
            switch (selection)
            {
                case '1':
                    _operationsManager.StartMatrixOperations();
                    break;
                case '2':
                    _fileManager.StartFileManager();
                    break;
                case '3':
                    _assemblyManager.ShowAssemblyInformation();
                    break;
                case '4':
                    ShowAuthorInfo();
                    break;
                default:
                    Console.WriteLine("Wrong selection. Press correct number.\n");
                    break;
            }
        }

        private void ShowAuthorInfo()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Author: Wojciech Dąbrowski, OS1\n");
            Console.ResetColor();
        }

        private void ShowMenu()
        {
            Console.WriteLine("MENU:\n" +
                              "1. Matrix operations\n" +
                              "2. I/O operations\n" +
                              "3. Show library information\n" +
                              "4. Show author info\n" +
                              "5. Exit\n\n" +
                              "Press selected number\n");
        }
    }
}
