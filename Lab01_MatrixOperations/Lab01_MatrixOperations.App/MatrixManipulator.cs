using System;
using Lab01_MatrixOperations.Library;

namespace Lab01_MatrixOperations.App
{
    internal class MatrixManipulator
    {
        internal MathMatrix Add(MathMatrix firstComponent, MathMatrix secondComponent)
        {
            MathMatrix result = null;
            ShowMatrix(firstComponent);
            ShowMatrix(secondComponent);

            try
            {
                result = firstComponent + secondComponent;
                ShowMatrix(result);
            }
            catch (Exception exception)
            {
                Console.WriteLine("An error has occured: " + exception.Message);
            }
                       
            return result;
        }

        internal MathMatrix Substract(MathMatrix firstComponent, MathMatrix secondComponent)
        {
            MathMatrix result = null;
            ShowMatrix(firstComponent);
            ShowMatrix(secondComponent);

            try
            {
                result = firstComponent - secondComponent;
                ShowMatrix(result);
            }
            catch (Exception exception)
            {
                Console.WriteLine("An error has occured: " + exception.Message);
            }

            return result;
        }

        internal MathMatrix Multiplicate(MathMatrix firstComponent, MathMatrix secondComponent)
        {
            MathMatrix result = null;
            ShowMatrix(firstComponent);
            ShowMatrix(secondComponent);

            try
            {
                result = firstComponent * secondComponent;
                ShowMatrix(result);
            }
            catch (Exception exception)
            {
                Console.WriteLine("An error has occured: " + exception.Message);
            }

            return result;
        }

        internal void ShowMatrix(MathMatrix matrix)
        {
            Console.Write("{0} {1}\n", matrix.Rows, matrix.Columns);

            for (int i = 0; i < matrix.Rows; i++)
            {
                for (int j = 0; j < matrix.Columns; j++)
                {
                    Console.Write("{0:0.000}\t", matrix[i, j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        public MathMatrix GetNewMatrixFromUser()
        {
            int rows = 0;
            int columns = 0;

            Console.WriteLine("Insert row numbers: ");
            string insertedRows = Console.ReadLine();
            int.TryParse(insertedRows, out rows);

            Console.WriteLine("Insert columns number: ");
            string insertedColumns = Console.ReadLine();
            int.TryParse(insertedColumns, out columns);

            MathMatrix matrix = new MathMatrix(rows, columns);

            if (matrix.Rows < 1 || matrix.Columns < 1)
                return null;

            for (int i = 0; i < matrix.Rows; i++)
            {
                for (int j = 0; j < matrix.Columns; j++)
                {
                    Console.WriteLine("Insert [{0}, {1}] value: ", i, j);
                    string insertedValue = Console.ReadLine();
                    double value = 0;
                    double.TryParse(insertedValue, out value);
                    matrix[i, j] = value;
                }
            }

            return matrix;
        }
    }
}