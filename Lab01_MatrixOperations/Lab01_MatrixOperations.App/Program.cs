﻿using System;

namespace Lab01_MatrixOperations.App
{
    internal class Program
    {
        [STAThread]
        static void Main()
        {
            ProgramManager programManager = new ProgramManager();

            programManager.StartProgram();
        }
    }
}
