﻿using System;
using System.Collections.Generic;
using Lab01_MatrixOperations.Library;
using System.Windows.Forms;
using System.IO;

namespace Lab01_MatrixOperations.App
{
    internal class FileManager
    {
        private MathMatrix _mathMatrix;
        private MatrixManipulator _matrixManipulator;
        private OpenFileDialog _openFileDialog;
        private SaveFileDialog _saveFileDialog;

        public FileManager()
        {
            _mathMatrix = new MathMatrix(3, 3);

            _matrixManipulator = new MatrixManipulator();

            _openFileDialog = new OpenFileDialog();

            _saveFileDialog = new SaveFileDialog();
            _saveFileDialog.CreatePrompt = true;
            _saveFileDialog.OverwritePrompt = true;
            _saveFileDialog.DefaultExt = "txt";
            _saveFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";

            Random randomGenerator = new Random();

            for (int i = 0; i < _mathMatrix.Rows; i++)           
                for (int j = 0; j < _mathMatrix.Columns; j++)               
                    _mathMatrix[i, j] = randomGenerator.NextDouble();                          
        }

        private void ManageSelection(char selection)
        {
            MathMatrix tempMatrix;

            switch (selection)
            {
                case '1':
                    tempMatrix = ReadMatrixFromFile();
                    if (tempMatrix != null)
                        _mathMatrix = tempMatrix;
                    break;
                case '2':
                    WriteMatrixToFile(_mathMatrix);
                    break;
                case '3':
                    tempMatrix = _matrixManipulator.GetNewMatrixFromUser();
                    if (tempMatrix != null)
                        _mathMatrix = tempMatrix;
                    break;
                default:
                    Console.WriteLine("Wrong selection. Press correct number.\n");
                    break;
            }
        }

        private void WriteMatrixToFile(MathMatrix mathMatrix)
        {
            try
            {
                if (_saveFileDialog.ShowDialog() == DialogResult.OK)
                    using (StreamWriter streamWriter = new StreamWriter(_saveFileDialog.FileName))
                    {
                        streamWriter.Write("{0} {1}", mathMatrix.Rows, mathMatrix.Columns);
                        streamWriter.WriteLine();

                        for (int i = 0; i < mathMatrix.Rows; i++)
                        {
                            for (int j = 0; j < mathMatrix.Columns; j++)
                            {
                                streamWriter.Write("{0:0.000}", mathMatrix[i, j]);
                                if(j != mathMatrix.Columns - 1)
                                    streamWriter.Write("\t");
                            }
                            streamWriter.WriteLine();
                        }
                    }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error with writing file:\n{0}", ex.Message);
            }
        }

        private MathMatrix ReadMatrixFromFile()
        {
            MathMatrix readMatrix = null;
            List<string> lines = new List<string>();

            try
            {
                if (_openFileDialog.ShowDialog() == DialogResult.OK)
                    using (StreamReader streamReader = new StreamReader(_openFileDialog.FileName))
                        while (!streamReader.EndOfStream)
                            lines.Add(streamReader.ReadLine());

                for (int i = 0; i < lines.Count; i++)
                {
                    string[] splitedNumbers = lines[i].Split(null);
                    if (i == 0)
                    {
                        if (splitedNumbers.Length != 2)
                        {
                            throw new ArgumentException("Given matrix has wrong parameters");
                        }

                        int rows = 0;
                        bool rowsSuccess = int.TryParse(splitedNumbers[0], out rows);
                        int columns = 0;
                        bool columnsSuccess = int.TryParse(splitedNumbers[1], out columns);

                        readMatrix = new MathMatrix(rows, columns);
                    }
                    else
                    {
                        for (int j = 0; j < splitedNumbers.Length; j++)
                        {
                            double numberFromFile = 0;
                            double.TryParse(splitedNumbers[j], out numberFromFile);
                            readMatrix[i - 1, j] = numberFromFile;
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine("Error with opening file:\n{0}", ex.Message);
            }

            return readMatrix;
        }

        private void ShowMatrixAndMenu()
        {
            Console.WriteLine("Matrix:\n");
            _matrixManipulator.ShowMatrix(_mathMatrix);

            Console.WriteLine("MENU:\n" +
                              "1. Read matrix from file\n" +
                              "2. Write matrix to file\n" +
                              "3. Change matrix\n" +
                              "4. Exit\n\n" +
                              "Press selected number\n");
        }

        public void StartFileManager()
        {
            char selection = char.MinValue;

            while (selection != '4')
            {
                ShowMatrixAndMenu();
                selection = Console.ReadKey().KeyChar;
                Console.WriteLine();

                Console.Clear();
                ManageSelection(selection);
            }
        }
    }
}