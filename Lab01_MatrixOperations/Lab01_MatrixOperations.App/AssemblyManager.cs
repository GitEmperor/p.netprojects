﻿using System;
using System.Linq;
using System.Reflection;
using Lab01_MatrixOperations.Library;

namespace Lab01_MatrixOperations.App
{
    internal class AssemblyManager
    {
        internal void ShowAssemblyInformation()
        {
            Assembly assembly = typeof (MathMatrix).Assembly;
            ShowGeneralAssemblyInformation(assembly);
            ShowTypesAndMembersInfo(assembly);
        }

        private void ShowGeneralAssemblyInformation(Assembly assembly)
        {
            Console.WriteLine(assembly.FullName);

            var companyAttribute = assembly
                .GetCustomAttributes(typeof(AssemblyCompanyAttribute), false)
                .OfType<AssemblyCompanyAttribute>()
                .FirstOrDefault();

            var descriptionAttribute = assembly
                .GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false)
                .OfType<AssemblyDescriptionAttribute>()
                .FirstOrDefault();

            if (descriptionAttribute != null)
                Console.WriteLine("Description: {0}", descriptionAttribute.Description);
            if (companyAttribute != null)
                Console.WriteLine("Author: {0}", companyAttribute.Company);

            Console.WriteLine();
        }

        private static void ShowTypesAndMembersInfo(Assembly assembly)
        {
            foreach (Type type in assembly.GetTypes())
            {
                var methodsInfo = type.GetMethods();
                var propertiesInfo = type.GetProperties();

                Console.WriteLine("{0} {1} {2}", GetAccessModifierForType(type), GetTypeKind(type), type.Name);

                foreach (MethodInfo methodInfo in methodsInfo)
                {
                    Console.WriteLine("{0} {1}", GetAccessModifierForMethod(methodInfo), methodInfo);
                }
                foreach (PropertyInfo propertyInfo in propertiesInfo)
                {
                    Console.WriteLine("public {0}", propertyInfo.Name);
                }
                Console.WriteLine();
            }
        }

        private static string GetAccessModifierForMethod(MethodInfo methodInfo)
        {
            if (methodInfo.IsPublic)
                return "public";
            if (methodInfo.IsPrivate)
                return "private";
            if(methodInfo.IsFamily)
                return "protected";
            if (methodInfo.IsAssembly)
                return "internal";

            return string.Empty;
        }

        private static string GetAccessModifierForType(Type type)
        {
            if (type.IsPublic)
                return "public";
            if (type.IsAbstract)
                return "abstract";

            return string.Empty;
        }

        private static string GetTypeKind(Type type)
        {
            if (type.IsClass)
                return "class ";
            if (type.IsInterface)
                return "interface ";
            if (type.IsEnum)
                return "enum ";

            return string.Empty;
        }
    }
}
