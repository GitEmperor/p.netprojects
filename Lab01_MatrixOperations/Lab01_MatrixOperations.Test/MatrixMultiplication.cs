﻿using System;
using Lab01_MatrixOperations.Library;
using NUnit.Framework;

namespace Lab01_MatrixOperations.Test
{
    [TestFixture]
    public class MatrixMultiplication
    {
        private static object[] CorrectMultiplicateTestCases =
        {
            new object[]
            {
                new MathMatrix(new double[,] {{-4, 1, 6, 0}, {-3, -2, 4, 2}, {3, 5, 7, 8}}),
                new MathMatrix(new double[,] {{-4, -1, 1, 6}, {-3, 9, 4, 10}, {0, -2, 2, 3}, {5, 7, 8, 11}}),
                new MathMatrix(new double[,] {{13, 1, 12, 4}, {28, -9, 13, -4}, {13, 84, 101, 177}})
            },

            new object[]
            {
                new MathMatrix(new double[,] {{5, -1, 1}, {0, 6, 4}, {7, -2, 2}}),
                new MathMatrix(new double[,] {{3, 5, 8}, {1, -1, -4}, {0, -3, 4}}),
                new MathMatrix(new double[,] {{14, 23, 48}, {6, -18, -8}, {19, 31, 72}})
            },

            new object[]
            {
                new MathMatrix(new double[,] {{6, -2, -1}, {1, 5, -4}}),
                new MathMatrix(new double[,] {{-3, -2, 4, 0}, {-1, 1, 5, 6}, {7, -4, 2, 3}}),
                new MathMatrix(new double[,] {{-23, -10, 12, -15}, {-36, 19, 21, 18}})
            },
            new object[]
            {
                new MathMatrix(new double[,] {{4, 6, -2, 5}}),
                new MathMatrix(new double[,] {{7}, {8}, {9}, {2}}),
                new MathMatrix(new double[,] {{68}})
            },
            new object[]
            {
                new MathMatrix(new double[,] {{-1, 5}, {2, 1}, {-3, 3}, {6, -2}}),
                new MathMatrix(new double[,] {{7, -1, 5}, {2, 1, 4}}),
                new MathMatrix(new double[,] {{3, 6, 15}, {16, -1, 14}, {-15, 6, -3}, {38, -8, 22}})
            }
        };

        private static object[] IncorrectMultiplicateTestCases =
        {
            new object[]
            {
                new MathMatrix(new double[,] {{1, -4, 5}, {0, 2, -1}, {4, 2, 1}, {2, 4, -6}}),
                new MathMatrix(new double[,] {{3, 1, 2}, {-2, 0, 1}, {-3, 6, 0}, {2, 4, -6}})
            },

            new object[]
            {
                new MathMatrix(new double[,] {{-1, 2, 6}, {4, 6, -8}}),
                new MathMatrix(new double[,] {{3, 9, 8}, {1, 0, -1}, {2, 7, 8}, {2, 7, 8}})
            },

            new object[]
            {
                new MathMatrix(new double[,] {{20, -18, 0}, {4, 5, -9}}),
                new MathMatrix(new double[,] {{1, 15}, {5, -16}})
            },

            new object[]
            {
                new MathMatrix(new double[,] {{1, 15, -2}, {5, -16, 1}, {20, -18, 1}}),
                new MathMatrix(new double[,] {{20, -18, 1}, {4, 5, 2}})
            },
        };

        [Test, TestCaseSource(nameof(CorrectMultiplicateTestCases))]
        public void MultiplicateTwoCorrectSizedMatricesShouldReturnProperValue(MathMatrix firstComponent,
            MathMatrix secondComponent, MathMatrix expectedResult)
        {
            // when
            MathMatrix result = MultiplicateTwoMatrices(firstComponent, secondComponent);

            // then
            Assert.That(result, Is.EqualTo(expectedResult));
        }

        [Test, TestCaseSource(nameof(IncorrectMultiplicateTestCases))]
        public void MultiplicateMatricesWithDifferentSizesShouldThrowArgumentException(MathMatrix firstComponent,
            MathMatrix secondComponent)
        {
            Assert.Throws<ArgumentException>(
                () => { MultiplicateTwoMatrices(firstComponent, secondComponent); });
        }

        private MathMatrix MultiplicateTwoMatrices(MathMatrix firstComponent, MathMatrix secondComponent)
        {
            return firstComponent*secondComponent;
        }
    }
}
