﻿using System;
using Lab01_MatrixOperations.Library;
using NUnit.Framework;

namespace Lab01_MatrixOperations.Test
{
    [TestFixture]
    public class MatrixSubstraction
    {
        private static object[] CorrectSubstractionTestCases =
        {
            new object[]
            {
                new MathMatrix(new double[,] {{1, -4, 5}, {0, 2, -1}, {4, 2, 1}, {2, 4, -6}}),
                new MathMatrix(new double[,] {{3, 1, 2}, {-2, 0, 1}, {-3, 6, 0}, {-2, 1, 5}}),
                new MathMatrix(new double[,] {{-2, -5, 3}, {2, 2, -2}, {7, -4, 1}, {4, 3, -11}})
            },

            new object[]
            {
                new MathMatrix(new double[,] {{1, 3, 2}, {-1, 2, 6}, {4, 6, -8}}),
                new MathMatrix(new double[,] {{3, 9, 8}, {1, 0, -1}, {2, 7, 8}}),
                new MathMatrix(new double[,] {{-2, -6, -6}, {-2, 2, 7}, {2, -1, -16}})
            },

            new object[]
            {
                new MathMatrix(new double[,] {{1, 15, -20}, {5, -16, 1}}),
                new MathMatrix(new double[,] {{20, -18, 0}, {4, 5, -9}}),
                new MathMatrix(new double[,] {{-19, 33, -20}, {1, -21, 10}})
            },
        };

        private static object[] IncorrectSubstractionTestCases =
        {
            new object[]
            {
                new MathMatrix(new double[,] {{1, -4, 5}, {0, 2, -1}, {4, 2, 1}, {2, 4, -6}}),
                new MathMatrix(new double[,] {{3, 1, 2}, {-2, 0, 1}, {-3, 6, 0}})
            },

            new object[]
            {
                new MathMatrix(new double[,] {{-1, 2, 6}, {4, 6, -8}}),
                new MathMatrix(new double[,] {{3, 9, 8}, {1, 0, -1}, {2, 7, 8}})
            },

            new object[]
            {
                new MathMatrix(new double[,] {{1, 15}, {5, -16}}),
                new MathMatrix(new double[,] {{20, -18, 0}, {4, 5, -9}})
            },

            new object[]
            {
                new MathMatrix(new double[,] {{1, 15, -2}, {5, -16, 1}}),
                new MathMatrix(new double[,] {{20, -18}, {4, 5}})
            },
        };

        [Test, TestCaseSource(nameof(CorrectSubstractionTestCases))]
        public void SubstractTwoCorrectSizedMatricesShouldReturnProperValue(MathMatrix firstComponent,
            MathMatrix secondComponent, MathMatrix expectedResult)
        {
            // when
            MathMatrix result = SubstractTwoMatrices(firstComponent, secondComponent);

            // then
            Assert.That(result, Is.EqualTo(expectedResult));
        }

        [Test, TestCaseSource(nameof(IncorrectSubstractionTestCases))]
        public void SubstractMatricesWithDifferentSizesShouldThrowArgumentException(MathMatrix firstComponent,
            MathMatrix secondComponent)
        {
            Assert.Throws<ArgumentException>(
                () => { SubstractTwoMatrices(firstComponent, secondComponent); });
        }

        private MathMatrix SubstractTwoMatrices(MathMatrix firstComponent, MathMatrix secondComponent)
        {
            return firstComponent - secondComponent;
        }
    }
}
