﻿using NUnit.Framework;
using System;
using Lab01_MatrixOperations.Library;

namespace Lab01_MatrixOperations.Test
{
    [TestFixture]
    public class MatrixAddition
    {
        private static object[] CorrectAddingTestCases =
        {
            new object[]
            {
                new MathMatrix(new double[,] {{1, -4, 5}, {0, 2, -1}, {4, 2, 1}, {2, 4, -6}}),
                new MathMatrix(new double[,] {{3, 1, 2}, {-2, 0, 1}, {-3, 6, 0}, {-2, 1, 5}}),
                new MathMatrix(new double[,] {{4, -3, 7}, {-2, 2, 0}, {1, 8, 1}, {0, 5, -1}})
            },

            new object[]
            {
                new MathMatrix(new double[,] {{1, 3, 2}, {-1, 2, 6}, {4, 6, -8}}),
                new MathMatrix(new double[,] {{3, 9, 8}, {1, 0, -1}, {2, 7, 8}}),
                new MathMatrix(new double[,] {{4, 12, 10}, {0, 2, 5}, {6, 13, 0}})
            },

            new object[]
            {
                new MathMatrix(new double[,] {{1, 15, -20}, {5, -16, 1}}),
                new MathMatrix(new double[,] {{20, -18, 0}, {4, 5, -9}}),
                new MathMatrix(new double[,] {{21, -3, -20}, {9, -11, -8}})
            },
        };

        private static object[] IncorrectAddingTestCases =
        {
            new object[]
            {
                new MathMatrix(new double[,] {{1, -4, 5}, {0, 2, -1}, {4, 2, 1}, {2, 4, -6}}),
                new MathMatrix(new double[,] {{3, 1, 2}, {-2, 0, 1}, {-3, 6, 0}})
            },

            new object[]
            {
                new MathMatrix(new double[,] {{-1, 2, 6}, {4, 6, -8}}),
                new MathMatrix(new double[,] {{3, 9, 8}, {1, 0, -1}, {2, 7, 8}})
            },

            new object[]
            {
                new MathMatrix(new double[,] {{1, 15}, {5, -16}}),
                new MathMatrix(new double[,] {{20, -18, 0}, {4, 5, -9}})
            },

            new object[]
            {
                new MathMatrix(new double[,] {{1, 15, -2}, {5, -16, 1}}),
                new MathMatrix(new double[,] {{20, -18}, {4, 5}})
            },
        };

        [Test, TestCaseSource(nameof(CorrectAddingTestCases))]
        public void AddingTwoCorrectSizedMatricesShouldReturnProperValue(MathMatrix firstComponent,
            MathMatrix secondComponent, MathMatrix expectedResult)
        {
            // when
            MathMatrix result = AddTwoMatrices(firstComponent, secondComponent);

            // then
            Assert.That(result, Is.EqualTo(expectedResult));
        }

        [Test, TestCaseSource(nameof(IncorrectAddingTestCases))]
        public void AddingMatricesWithDifferentSizesShouldThrowArgumentException(MathMatrix firstComponent,
            MathMatrix secondComponent)
        {
            Assert.Throws<ArgumentException>(
                () => { AddTwoMatrices(firstComponent, secondComponent); });
        }

        private MathMatrix AddTwoMatrices(MathMatrix firstComponent, MathMatrix secondComponent)
        {
            return firstComponent + secondComponent;
        }
    }
}
