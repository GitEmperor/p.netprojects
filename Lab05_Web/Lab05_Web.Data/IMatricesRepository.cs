﻿using System.Collections.Generic;
using Lab05_Web.Data.Models;
using Lab05_Web.Data.ViewModels;

namespace Lab05_Web.Data
{
    public interface IMatricesRepository
    {
        Matrix GetMatrixById(int id);

        MatrixOperation GetMatrixOperationById(int id);

        List<Matrix> GetAllMatrices();

        List<MatrixOperationViewModel> GetAllOperations();

        void WriteOperation(MatrixOperation operation);

        void WriteMatrix(Matrix matrix);
    }
}
