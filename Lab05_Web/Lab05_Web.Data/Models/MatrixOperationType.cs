﻿using System;

namespace Lab05_Web.Data.Models
{
    [Serializable]
    public enum MatrixOperationType
    {
        Add,
        Substract,
        Multiply
    }
}
