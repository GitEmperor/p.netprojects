﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Lab05_Web.Data.Models
{
    [Serializable]
    public class Matrix
    {
        [Key]
        public int MatrixId { get; set; }

        public DateTime CreationDate { get; set; }

        public string MatrixContent { get; set; }
    }
}
