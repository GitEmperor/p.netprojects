﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using Lab05_Web.Data.Models;
using Lab05_Web.Data.ViewModels;

namespace Lab05_Web.Data
{
    /// <summary>
    /// Summary description for MatricesRepositoryService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class MatricesRepositoryService : WebService, IMatricesRepository
    {
        [WebMethod]
        public List<Matrix> GetAllMatrices()
        {
            using (MatrixContext context = new MatrixContext())
            {
                return context.Matrices.ToList();
            }
        }

        [WebMethod]
        public List<MatrixOperationViewModel> GetAllOperations()
        {
            using (MatrixContext context = new MatrixContext())
            {
                return context.MatrixOperations.Select(contextMatrixOperation => new MatrixOperationViewModel
                {
                    OperationDate = contextMatrixOperation.OperationDate,
                    OperationType = contextMatrixOperation.OperationType,
                    FirstMatrixContent = context.Matrices.FirstOrDefault(
                        x => x.MatrixId == contextMatrixOperation.FirstMatrix.MatrixId).MatrixContent,
                    SecondMatrixContent = context.Matrices.FirstOrDefault(
                        x => x.MatrixId == contextMatrixOperation.SecondMatrix.MatrixId).MatrixContent,
                    ResultMatrixContent = context.Matrices.FirstOrDefault(
                        x => x.MatrixId == contextMatrixOperation.ResultMatrix.MatrixId).MatrixContent
                }).ToList();
            }
        }

        [WebMethod]
        public Matrix GetMatrixById(int id)
        {
            using (MatrixContext context = new MatrixContext())
            {
                return context.Matrices.FirstOrDefault(x => x.MatrixId == id);
            }
        }

        [WebMethod]
        public MatrixOperation GetMatrixOperationById(int id)
        {
            using (MatrixContext context = new MatrixContext())
            {
                return context.MatrixOperations.FirstOrDefault(x => x.OperationId == id);
            }
        }

        [WebMethod]
        public void WriteMatrix(Matrix matrix)
        {
            using (MatrixContext context = new MatrixContext())
            {
                context.Matrices.Add(matrix);
                context.SaveChanges();
            }
        }

        [WebMethod]
        public void WriteOperation(MatrixOperation operation)
        {
            using (MatrixContext context = new MatrixContext())
            {
                context.MatrixOperations.Add(operation);
                context.SaveChanges();
            }
        }
    }
}
