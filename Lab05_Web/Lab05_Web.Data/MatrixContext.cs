﻿using System.Data.Entity;
using Lab05_Web.Data.Models;

namespace Lab05_Web.Data
{
    public class MatrixContext : DbContext
    {
        public DbSet<Matrix> Matrices { get; set; }

        public DbSet<MatrixOperation> MatrixOperations { get; set; }
    }
}
