﻿using System;
using Lab05_Web.Data.Models;

namespace Lab05_Web.Data.ViewModels
{
    [Serializable]
    public class MatrixOperationViewModel
    {
        public DateTime OperationDate { get; set; }

        public MatrixOperationType OperationType { get; set; }

        public string FirstMatrixContent { get; set; }

        public string SecondMatrixContent { get; set; }

        public string ResultMatrixContent { get; set; }
    }
}
