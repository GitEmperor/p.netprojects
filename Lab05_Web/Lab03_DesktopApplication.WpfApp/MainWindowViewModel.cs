﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using Lab01_MatrixOperations.Library;
using System.Windows;
using Lab03_DesktopApplication.WpfApp.localhost;
using Lab03_DesktopApplication.WpfApp.localhost1;
using Lab05_Web.Common;

namespace Lab03_DesktopApplication.WpfApp
{
    internal class MainWindowViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        #region Commands

        public ICommand LoadFirstMatrix { get; set; }
        public ICommand LoadSecondMatrix { get; set; }
        public ICommand SaveResultMatrix { get; set; }
        public ICommand ExitApplication { get; set; }
        public ICommand DisplayAboutInformation { get; set; }
        public ICommand DisplayOperationHistory { get; set; }
        public ICommand Calculate { get; set; }
        public ICommand DatabaseLoadFirstMatrix { get; set; }
        public ICommand DatabaseLoadSecondMatrix { get; set; }
        public ICommand DatabaseSaveFirstMatrix { get; set; }
        public ICommand DatabaseSaveSecondMatrix { get; set; }
        public ICommand DatabaseSaveResultMatrix { get; set; }

        #endregion

        #region Properties

        public bool SaveResultButtonIsEnabled
        {
            get { return _saveResultButtonIsEnabled; }
            set
            {
                _saveResultButtonIsEnabled = value;
                OnPropertyChanged("SaveResultButtonIsEnabled");
            }
        }

        public bool DatabaseSaveFirstMatrixButtonIsEnabled
        {
            get { return _databaseSaveFirstMatrixButtonIsEnabled; }
            set
            {
                _databaseSaveFirstMatrixButtonIsEnabled = value;
                OnPropertyChanged("DatabaseSaveFirstMatrixButtonIsEnabled");
            }
        }

        public bool DatabaseSaveSecondMatrixButtonIsEnabled
        {
            get { return _databaseSaveSecondMatrixButtonIsEnabled; }
            set
            {
                _databaseSaveSecondMatrixButtonIsEnabled = value;
                OnPropertyChanged("DatabaseSaveSecondMatrixButtonIsEnabled");
            }
        }

        public bool CalculateButtonIsEnabled
        {
            get { return _calculateButtonIsEnabled; }
            set
            {
                _calculateButtonIsEnabled = value;
                OnPropertyChanged("CalculateButtonIsEnabled");
            }
        }

        public string FirstMatrixTextBoxText
        {
            get { return _firstMatrixTextBoxText; }
            set
            {
                _firstMatrixTextBoxText = value;
                OnPropertyChanged("FirstMatrixTextBoxText");
            }
        }

        public string SecondMatrixTextBoxText
        {
            get { return _secondMatrixTextBoxText; }
            set
            {
                _secondMatrixTextBoxText = value;
                OnPropertyChanged("SecondMatrixTextBoxText");
            }
        }

        public string ThirdMatrixTextBoxText
        {
            get { return _thirdMatrixTextBoxText; }
            set
            {
                _thirdMatrixTextBoxText = value;
                OnPropertyChanged("ThirdMatrixTextBoxText");
            }
        }

        public MatrixOperationType SelectedOperation
        {
            get { return _selectedOperation; }
            set
            {
                _selectedOperation = value;
                OnPropertyChanged("SelectedOperation");
            }
        }

        #endregion

        #region Constructors

        public MainWindowViewModel()
        {
            LoadFirstMatrix = new RelayCommand(LoadFirstMatrixExecute, CanCommandExecute);
            LoadSecondMatrix = new RelayCommand(LoadSecondMatrixExecute, CanCommandExecute);
            SaveResultMatrix = new RelayCommand(SaveResultMatrixExecute, CanCommandExecute);
            ExitApplication = new RelayCommand(ExitApplicationExecute, CanCommandExecute);
            DisplayAboutInformation = new RelayCommand(DisplayAboutInformationExecute, CanCommandExecute);
            DisplayOperationHistory = new RelayCommand(DisplayOperationHistoryExecute, CanCommandExecute);
            DatabaseLoadFirstMatrix = new RelayCommand(DatabaseLoadFirstMatrixExecute, CanCommandExecute);
            DatabaseLoadSecondMatrix = new RelayCommand(DatabaseLoadSecondMatrixExecute, CanCommandExecute);
            DatabaseSaveFirstMatrix = new RelayCommand(DatabaseSaveFirstMatrixExecute, CanCommandExecute);
            DatabaseSaveSecondMatrix = new RelayCommand(DatabaseSaveSecondMatrixExecute, CanCommandExecute);
            DatabaseSaveResultMatrix = new RelayCommand(DatabaseSaveResultMatrixExecute, CanCommandExecute);
            Calculate = new RelayCommand(CalculateExecute, CanCommandExecute);

            _backgroundWorker = new BackgroundWorker();
            _backgroundWorker.DoWork += MatrixCalculation;
            matrixCalculatorService = new MatrixCalculatorService();
            _matricesRepositoryService = new MatricesRepositoryService();

            SaveResultButtonIsEnabled = false;
            CalculateButtonIsEnabled = false;
            _databaseSaveFirstMatrixButtonIsEnabled = false;
            _databaseSaveSecondMatrixButtonIsEnabled = false;
            SelectedOperation = MatrixOperationType.Add;
        }

        #endregion

        #region Fields

        private MathMatrix _firstMatrix;
        private MathMatrix _secondMatrix;
        private MathMatrix _thirdMatrix;
        private BackgroundWorker _backgroundWorker;
        private bool _saveResultButtonIsEnabled;
        private bool _calculateButtonIsEnabled;
        private bool _databaseSaveFirstMatrixButtonIsEnabled;
        private bool _databaseSaveSecondMatrixButtonIsEnabled;
        private string _firstMatrixTextBoxText;
        private string _secondMatrixTextBoxText;
        private string _thirdMatrixTextBoxText;
        private MatrixCalculatorService matrixCalculatorService;
        private MatrixOperationType _selectedOperation;
        private MatricesRepositoryService _matricesRepositoryService;

        #endregion

        private void MatrixCalculation(object sender, DoWorkEventArgs e)
        {
            try
            {
                SaveResultButtonIsEnabled = false;
                CalculateButtonIsEnabled = false;
                _databaseSaveFirstMatrixButtonIsEnabled = false;
                _databaseSaveSecondMatrixButtonIsEnabled = false;

                switch (SelectedOperation)
                {
                    case MatrixOperationType.Add:
                        _thirdMatrix = new MathMatrix
                            (
                                matrixCalculatorService.AddMatrices(
                                    _firstMatrix.ToArray().ToJaggedArray(),
                                    _secondMatrix.ToArray().ToJaggedArray())
                                .ToMultidimensionalArray()
                            );
                        break;
                    case MatrixOperationType.Substract:
                        _thirdMatrix = new MathMatrix
                            (
                                matrixCalculatorService.SubstractMatrices(
                                    _firstMatrix.ToArray().ToJaggedArray(),
                                    _secondMatrix.ToArray().ToJaggedArray())
                                .ToMultidimensionalArray()
                            );
                        break;
                    case MatrixOperationType.Multiply:
                        _thirdMatrix = new MathMatrix
                            (
                                matrixCalculatorService.MultipleMatrices(
                                    _firstMatrix.ToArray().ToJaggedArray(),
                                    _secondMatrix.ToArray().ToJaggedArray())
                                .ToMultidimensionalArray()
                            );
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                _matricesRepositoryService.WriteOperation(new MatrixOperation
                {
                    FirstMatrix = new Matrix { CreationDate = DateTime.Now, MatrixContent = MatrixManipulator.ConvertMatrixToFormattedString(_firstMatrix) },
                    SecondMatrix = new Matrix { CreationDate = DateTime.Now, MatrixContent = MatrixManipulator.ConvertMatrixToFormattedString(_secondMatrix) },
                    ResultMatrix = new Matrix { CreationDate = DateTime.Now, MatrixContent = MatrixManipulator.ConvertMatrixToFormattedString(_thirdMatrix) },
                    OperationDate = DateTime.Now,
                    OperationType = SelectedOperation
                });

                string fullMatrixString = MatrixManipulator.ConvertMatrixToFormattedString(_thirdMatrix);

                ThirdMatrixTextBoxText = fullMatrixString.Remove(0, fullMatrixString.IndexOf(Environment.NewLine));
            }
            catch (Exception ex)
            {
                MessageBoxManager.ShowDetailedErrorMessageBox("Error with matrix calculations", ex);
            }
            finally
            {
                RefreshButtons();
            }
        }

        private void RefreshButtons()
        {
            if (_firstMatrix != null && _secondMatrix != null)
            {
                CalculateButtonIsEnabled = true;
            }
            else
            {
                CalculateButtonIsEnabled = false;
            }

            DatabaseSaveFirstMatrixButtonIsEnabled = _firstMatrix != null;
            DatabaseSaveSecondMatrixButtonIsEnabled = _secondMatrix != null;
            SaveResultButtonIsEnabled = _thirdMatrix != null;
        }

        #region CommandExecutions

        private void CalculateExecute()
        {
            _backgroundWorker.RunWorkerAsync();
        }

        private void DisplayAboutInformationExecute()
        {
            MessageBoxManager.ShowAboutMessageBox();
        }

        private void DisplayOperationHistoryExecute()
        {
            OperationHistoryWindow operationHistoryWindow = new OperationHistoryWindow(_matricesRepositoryService.GetAllOperations().ToList());
            operationHistoryWindow.Show();
        }

        private void ExitApplicationExecute()
        {
            Application.Current.Shutdown();
        }

        private void SaveResultMatrixExecute()
        {
            FileOperator.WriteMatrixToFile(_thirdMatrix);
        }

        private void LoadFirstMatrixExecute()
        {
            _firstMatrix = FileOperator.ReadMatrixFromFile();

            if (_firstMatrix != null)
            {
                string fullMatrixString = MatrixManipulator.ConvertMatrixToFormattedString(_firstMatrix);
                FirstMatrixTextBoxText = fullMatrixString.Remove(0, fullMatrixString.IndexOf(Environment.NewLine));
            }

            RefreshButtons();
        }

        private void LoadSecondMatrixExecute()
        {
            _secondMatrix = FileOperator.ReadMatrixFromFile();

            if (_secondMatrix != null)
            {
                string fullMatrixString = MatrixManipulator.ConvertMatrixToFormattedString(_secondMatrix);
                SecondMatrixTextBoxText = fullMatrixString.Remove(0, fullMatrixString.IndexOf(Environment.NewLine));
            }

            RefreshButtons();
        }

        private void DatabaseSaveResultMatrixExecute()
        {
            _matricesRepositoryService.WriteMatrix(new Matrix
            {
                CreationDate = DateTime.Now,
                MatrixContent = MatrixManipulator.ConvertMatrixToFormattedString(_thirdMatrix)
            });
        }

        private void DatabaseSaveSecondMatrixExecute()
        {
            _matricesRepositoryService.WriteMatrix(new Matrix
            {
                CreationDate = DateTime.Now,
                MatrixContent = MatrixManipulator.ConvertMatrixToFormattedString(_secondMatrix)
            });
        }

        private void DatabaseSaveFirstMatrixExecute()
        {
            _matricesRepositoryService.WriteMatrix(new Matrix
            {
                CreationDate = DateTime.Now,
                MatrixContent = MatrixManipulator.ConvertMatrixToFormattedString(_firstMatrix)
            });
        }

        private void DatabaseLoadFirstMatrixExecute()
        {
            MatrixSelectionWindow matrixSelectionWindow = new MatrixSelectionWindow(_matricesRepositoryService.GetAllMatrices().ToList());

            if (matrixSelectionWindow.ShowDialog() == false)
            {
                if (!string.IsNullOrEmpty(matrixSelectionWindow.SelectedMatrixContent))
                {
                    string fullMatrixString = matrixSelectionWindow.SelectedMatrixContent;
                    _firstMatrix = MatrixManipulator.ConvertStringLinesToMatrix(
                        fullMatrixString.Split(
                            new[] { Environment.NewLine }, StringSplitOptions.None).ToList());

                    FirstMatrixTextBoxText = fullMatrixString.Remove(0, fullMatrixString.IndexOf(Environment.NewLine));
                }
            }

            RefreshButtons();
        }

        private void DatabaseLoadSecondMatrixExecute()
        {
            MatrixSelectionWindow matrixSelectionWindow = new MatrixSelectionWindow(_matricesRepositoryService.GetAllMatrices().ToList());

            if (matrixSelectionWindow.ShowDialog() == false)
            {
                if (!string.IsNullOrEmpty(matrixSelectionWindow.SelectedMatrixContent))
                {
                    string fullMatrixString = matrixSelectionWindow.SelectedMatrixContent;
                    _secondMatrix = MatrixManipulator.ConvertStringLinesToMatrix(
                        fullMatrixString.Split(
                            new[] { Environment.NewLine }, StringSplitOptions.None).ToList());

                    SecondMatrixTextBoxText = fullMatrixString.Remove(0, fullMatrixString.IndexOf(Environment.NewLine));
                }
            }

            RefreshButtons();
        }

        #endregion

        private bool CanCommandExecute()
        {
            return true;
        }

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            handler?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
