﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Text;
using Lab03_DesktopApplication.WpfApp.localhost1;

namespace Lab03_DesktopApplication.WpfApp
{
    internal class MessageBoxManager
    {
        public static void ShowAboutMessageBox()
        {
            MessageBox.Show("Author:\n\nWojciech Dąbrowski", "About", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public static void ShowCommonErrorMessageBox(string message)
        {
            MessageBox.Show($"An error has occurred: \n {message}", "Error!",
                MessageBoxButton.OK, MessageBoxImage.Error);
        }

        public static void ShowDetailedErrorMessageBox(string message, Exception innerException)
        {
            MessageBox.Show($"An error has occurred: \n {message}: \n {innerException.Message}", "Error!",
                MessageBoxButton.OK, MessageBoxImage.Error);
        }

        public static void ShowOperationsHistory(IList<MatrixOperation> matrixOperationsList)
        {
            StringBuilder builder = new StringBuilder();

            foreach(var matrixOperation in matrixOperationsList)
            {
                builder.AppendLine($@"{matrixOperation.OperationId}     {matrixOperation.OperationDate}");
            }

            MessageBox.Show(builder.ToString(), "Operations history", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}
