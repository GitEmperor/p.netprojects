﻿using System.Text;
using Lab01_MatrixOperations.Library;
using System.Collections.Generic;
using System;
using System.Linq;

namespace Lab03_DesktopApplication.WpfApp
{
    internal static class MatrixManipulator
    {
        internal static string ConvertMatrixToFormattedString(MathMatrix matrix)
        {
            if (matrix == null)
                return string.Empty;

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append($"{matrix.Rows} {matrix.Columns}");
            stringBuilder.AppendLine();

            for (int i = 0; i < matrix.Rows; i++)
            {
                for (int j = 0; j < matrix.Columns; j++)
                {
                    stringBuilder.Append(string.Format("{0:0.000}", matrix[i, j]));
                    if (j != matrix.Columns - 1)
                        stringBuilder.Append("\t");
                }
                stringBuilder.AppendLine();
            }

            return stringBuilder.ToString();
        }

        internal static MathMatrix ConvertStringLinesToMatrix(IList<string> lines)
        {
            MathMatrix readMatrix = null;

            lines = lines.Where(s => !string.IsNullOrWhiteSpace(s)).ToList();

            for (int i = 0; i < lines.Count; i++)
            {
                // split numbers by whitespace
                string[] splitedNumbers = lines[i].Split(null);

                // create Matrix object from first line of matrix definition
                if (i == 0)
                {
                    if (splitedNumbers.Length != 2)
                    {
                        throw new ArgumentException("Given matrix has wrong parameters");
                    }

                    int rows = 0;
                    int.TryParse(splitedNumbers[0], out rows);
                    int columns = 0;
                    int.TryParse(splitedNumbers[1], out columns);

                    readMatrix = new MathMatrix(rows, columns);
                }

                // fill matrix by numbers from input
                else
                {
                    for (int j = 0; j < splitedNumbers.Length; j++)
                    {
                        double numberFromFile = 0;
                        double.TryParse(splitedNumbers[j], out numberFromFile);
                        readMatrix[i - 1, j] = numberFromFile;
                    }
                }
            }

            return readMatrix;
        }
    }
}