﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Lab03_DesktopApplication.WpfApp.localhost1;

namespace Lab03_DesktopApplication.WpfApp
{
    /// <summary>
    /// Interaction logic for MatrixSelectionWindow.xaml
    /// </summary>
    public partial class MatrixSelectionWindow : Window
    {
        public string SelectedMatrixContent { get; set; }

        public MatrixSelectionWindow(List<Matrix> matrixList)
        {
            InitializeComponent();
            SelectedMatrixContent = string.Empty;
            MatricesDataGrid.ItemsSource = matrixList;
        }

        private void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataGridRow selectedMatrix = sender as DataGridRow;

            if (selectedMatrix != null)
                SelectedMatrixContent = (selectedMatrix.Item as Matrix).MatrixContent;

            Close();
        }
    }
}
