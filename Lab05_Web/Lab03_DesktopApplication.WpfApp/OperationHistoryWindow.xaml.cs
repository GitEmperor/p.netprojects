﻿using System.Collections.Generic;
using System.Windows;
using Lab03_DesktopApplication.WpfApp.localhost1;

namespace Lab03_DesktopApplication.WpfApp
{
    /// <summary>
    /// Interaction logic for OperationHistoryWindow.xaml
    /// </summary>
    public partial class OperationHistoryWindow : Window
    {
        public OperationHistoryWindow(List<MatrixOperationViewModel> operationsList)
        {
            InitializeComponent();
            OperationHistoryDataGrid.ItemsSource = operationsList;
        }
    }
}
