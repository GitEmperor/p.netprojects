﻿using System.Web.Services;
using Lab01_MatrixOperations.Library;
using Lab05_Web.Common;

namespace Lab05_Web.MatrixOperations
{
    /// <summary>
    /// Summary description for MatrixCalculatorService
    /// </summary>
    [WebService(Namespace = "http://matrixCalculator.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class MatrixCalculatorService : WebService
    {
        [WebMethod]
        public double[][] AddMatrices(double[][] firstMatrixAsArray, double[][] secondMatrixAsArray)
        {
            MathMatrix firstMatrix = new MathMatrix(firstMatrixAsArray.ToMultidimensionalArray());
            MathMatrix secondMatrix = new MathMatrix(secondMatrixAsArray.ToMultidimensionalArray());

            return (firstMatrix + secondMatrix).ToArray().ToJaggedArray();
        }

        [WebMethod]
        public double[][] SubstractMatrices(double[][] firstMatrixAsArray, double[][] secondMatrixAsArray)
        {
            MathMatrix firstMatrix = new MathMatrix(firstMatrixAsArray.ToMultidimensionalArray());
            MathMatrix secondMatrix = new MathMatrix(secondMatrixAsArray.ToMultidimensionalArray());

            return (firstMatrix - secondMatrix).ToArray().ToJaggedArray();
        }

        [WebMethod]
        public double[][] MultipleMatrices(double[][] firstMatrixAsArray, double[][] secondMatrixAsArray)
        {
            MathMatrix firstMatrix = new MathMatrix(firstMatrixAsArray.ToMultidimensionalArray());
            MathMatrix secondMatrix = new MathMatrix(secondMatrixAsArray.ToMultidimensionalArray());

            return (firstMatrix * secondMatrix).ToArray().ToJaggedArray();
        }
    }
}
