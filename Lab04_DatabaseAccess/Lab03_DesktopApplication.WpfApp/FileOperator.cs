﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mime;
using Lab01_MatrixOperations.Library;
using Microsoft.Win32;

namespace Lab03_DesktopApplication.WpfApp
{
    internal static class FileOperator
    {
        internal static MathMatrix ReadMatrixFromFile()
        {
            MathMatrix readMatrix = null;
            List<string> lines = new List<string>();
            OpenFileDialog openFileDialog = new OpenFileDialog();

            try
            {
                if (openFileDialog.ShowDialog() == true)
                    using (StreamReader streamReader = new StreamReader(openFileDialog.FileName))
                        while (!streamReader.EndOfStream)
                            lines.Add(streamReader.ReadLine());
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error with opening file:\n{0}", ex.Message);
                MessageBoxManager.ShowDetailedErrorMessageBox("Error with opening file", ex);
            }

            readMatrix = MatrixManipulator.ConvertStringLinesToMatrix(lines);

            return readMatrix;
        }

        internal static void WriteMatrixToFile(MathMatrix mathMatrix)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog
            {
                CreatePrompt = true,
                OverwritePrompt = true,
                DefaultExt = "txt",
                Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*"
            };

            try
            {
                if (saveFileDialog.ShowDialog() != true) return;

                string matrixString = MatrixManipulator.ConvertMatrixToFormattedString(mathMatrix);

                using (StreamWriter streamWriter = new StreamWriter(saveFileDialog.FileName))
                {
                    streamWriter.Write(matrixString);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error with writing file:\n{0}", ex.Message);
                MessageBoxManager.ShowDetailedErrorMessageBox("Error with opening file", ex);
            }
        }
    }
}