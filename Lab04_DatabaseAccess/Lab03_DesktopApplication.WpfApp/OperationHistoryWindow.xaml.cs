﻿using System.Collections.Generic;
using System.Windows;
using Lab04_DatabaseAccess.Data.ViewModels;

namespace Lab03_DesktopApplication.WpfApp
{
    /// <summary>
    /// Interaction logic for OperationHistoryWindow.xaml
    /// </summary>
    public partial class OperationHistoryWindow : Window
    {
        public OperationHistoryWindow(IList<MatrixOperationViewModel> operationsList)
        {
            InitializeComponent();
            OperationHistoryDataGrid.ItemsSource = operationsList;
        }
    }
}
