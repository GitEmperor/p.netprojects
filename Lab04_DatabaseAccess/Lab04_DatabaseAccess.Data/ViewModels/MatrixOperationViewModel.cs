﻿using Lab04_DatabaseAccess.Data.Models;
using System;

namespace Lab04_DatabaseAccess.Data.ViewModels
{
    public class MatrixOperationViewModel
    {
        public DateTime OperationDate { get; set; }

        public MatrixOperationType OperationType { get; set; }

        public string FirstMatrixContent { get; set; }

        public string SecondMatrixContent { get; set; }

        public string ResultMatrixContent { get; set; }
    }
}
