namespace Lab04_DatabaseAccess.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedMatrixContentType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Matrices", "MatrixContent", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Matrices", "MatrixContent");
        }
    }
}
