namespace Lab04_DatabaseAccess.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Matrices",
                c => new
                    {
                        MatrixId = c.Int(nullable: false, identity: true),
                        CreationDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.MatrixId);
            
            CreateTable(
                "dbo.MatrixOperations",
                c => new
                    {
                        OperationId = c.Int(nullable: false, identity: true),
                        OperationType = c.Int(nullable: false),
                        OperationDate = c.DateTime(nullable: false),
                        FirstMatrix_MatrixId = c.Int(),
                        ResultMatrix_MatrixId = c.Int(),
                        SecondMatrix_MatrixId = c.Int(),
                    })
                .PrimaryKey(t => t.OperationId)
                .ForeignKey("dbo.Matrices", t => t.FirstMatrix_MatrixId)
                .ForeignKey("dbo.Matrices", t => t.ResultMatrix_MatrixId)
                .ForeignKey("dbo.Matrices", t => t.SecondMatrix_MatrixId)
                .Index(t => t.FirstMatrix_MatrixId)
                .Index(t => t.ResultMatrix_MatrixId)
                .Index(t => t.SecondMatrix_MatrixId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MatrixOperations", "SecondMatrix_MatrixId", "dbo.Matrices");
            DropForeignKey("dbo.MatrixOperations", "ResultMatrix_MatrixId", "dbo.Matrices");
            DropForeignKey("dbo.MatrixOperations", "FirstMatrix_MatrixId", "dbo.Matrices");
            DropIndex("dbo.MatrixOperations", new[] { "SecondMatrix_MatrixId" });
            DropIndex("dbo.MatrixOperations", new[] { "ResultMatrix_MatrixId" });
            DropIndex("dbo.MatrixOperations", new[] { "FirstMatrix_MatrixId" });
            DropTable("dbo.MatrixOperations");
            DropTable("dbo.Matrices");
        }
    }
}
