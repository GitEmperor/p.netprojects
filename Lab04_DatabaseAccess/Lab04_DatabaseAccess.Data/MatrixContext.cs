﻿using Lab04_DatabaseAccess.Data.Models;
using System.Data.Entity;

namespace Lab04_DatabaseAccess.Data
{
    public class MatrixContext : DbContext
    {
        public DbSet<Matrix> Matrices { get; set; }

        public DbSet<MatrixOperation> MatrixOperations { get; set; }
    }
}
