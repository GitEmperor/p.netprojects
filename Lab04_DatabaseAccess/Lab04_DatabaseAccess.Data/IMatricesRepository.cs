﻿using Lab04_DatabaseAccess.Data.Models;
using Lab04_DatabaseAccess.Data.ViewModels;
using System.Collections.Generic;

namespace Lab04_DatabaseAccess.Data
{
    public interface IMatricesRepository
    {
        Matrix GetMatrixById(int id);

        MatrixOperation GetMatrixOperationById(int id);

        IList<Matrix> GetAllMatrices();

        IList<MatrixOperationViewModel> GetAllOperations();

        void WriteOperation(MatrixOperation operation);

        void WriteMatrix(Matrix matrix);

        void SaveChanges();
    }
}
