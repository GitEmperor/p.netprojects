﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lab04_DatabaseAccess.Data.Models;
using Lab04_DatabaseAccess.Data.ViewModels;

namespace Lab04_DatabaseAccess.Data
{
    public class MatricesRepository : IMatricesRepository
    {
        private readonly MatrixContext _context;

        public MatricesRepository()
        {
            _context = new MatrixContext();
        }

        public IList<Matrix> GetAllMatrices()
        {
            return _context.Matrices.ToList();
        }

        public IList<MatrixOperationViewModel> GetAllOperations()
        {
            return _context.MatrixOperations.Select(contextMatrixOperation => new MatrixOperationViewModel
                {
                    OperationDate = contextMatrixOperation.OperationDate,
                    OperationType = contextMatrixOperation.OperationType,
                    FirstMatrixContent = _context.Matrices.FirstOrDefault(
                        x => x.MatrixId == contextMatrixOperation.FirstMatrix.MatrixId).MatrixContent,
                    SecondMatrixContent = _context.Matrices.FirstOrDefault(
                        x => x.MatrixId == contextMatrixOperation.SecondMatrix.MatrixId).MatrixContent,
                    ResultMatrixContent = _context.Matrices.FirstOrDefault(
                        x => x.MatrixId == contextMatrixOperation.ResultMatrix.MatrixId).MatrixContent
                }).ToList();
        }

        public Matrix GetMatrixById(int id)
        {
            return _context.Matrices.FirstOrDefault(x => x.MatrixId == id);
        }

        public MatrixOperation GetMatrixOperationById(int id)
        {
            return _context.MatrixOperations.FirstOrDefault(x => x.OperationId == id);
        }

        public void WriteMatrix(Matrix matrix)
        {
            _context.Matrices.Add(matrix);
        }

        public void WriteOperation(MatrixOperation operation)
        {
            _context.MatrixOperations.Add(operation);
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}
