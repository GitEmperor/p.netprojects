﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Lab04_DatabaseAccess.Data.Models
{
    public class MatrixOperation
    {
        [Key]
        public int OperationId { get; set; }

        public Matrix FirstMatrix { get; set; }

        public Matrix SecondMatrix { get; set; }

        public Matrix ResultMatrix { get; set; }

        public MatrixOperationType OperationType { get; set; }

        public DateTime OperationDate { get; set; }
    }
}
