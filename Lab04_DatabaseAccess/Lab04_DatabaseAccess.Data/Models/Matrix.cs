﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Lab04_DatabaseAccess.Data.Models
{
    public class Matrix
    {
        [Key]
        public int MatrixId { get; set; }

        public DateTime CreationDate { get; set; }

        public string MatrixContent { get; set; }
    }
}
