﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;

namespace TheTripper.Models
{
    public class WorldUser : IdentityUser
    {
        public DateTime FirstTrip { get; set; }
    }
}