﻿using System.Diagnostics;

namespace TheTripper.Services
{
    public class DebugMailService : IMailService
    {
        public void SendMail(string to, string from, string subject, string body)
        {
            Debug.WriteLine($"Sending mail to: {to}, from: {from} with subject: {subject}");
        }
    }
}
