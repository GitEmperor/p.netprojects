﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Configuration;
using MimeKit;
using System.Collections.Generic;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace TheTripper.Services
{
    public class SmtpMailService : IMailService
    {
        private IConfigurationRoot _config;

        public SmtpMailService(IConfigurationRoot config)
        {
            _config = config;
        }

        async void IMailService.SendMail(string to, string from, string subject, string body)
        {
            string authenticationUserName = _config["Authentication:GoogleAuthentication:Username"];
            string authenticationPassword = _config["Authentication:GoogleAuthentication:Password"];
            var emailMessage = new MimeMessage();
            var senderAddress = new MailboxAddress(from);
            var recipients = new List<MailboxAddress> { new MailboxAddress(to)};

            emailMessage.From.Add(senderAddress);
            emailMessage.To.AddRange(recipients);
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart("plain") { Text = $"Message from: {from}\n\n{body}" };

            using (var client = new SmtpClient())
            {
                client.ServerCertificateValidationCallback =
                    delegate (object s, X509Certificate certificate, X509Chain chain,
                    SslPolicyErrors sslPolicyErrors)
                    { return true; };

                await client.ConnectAsync("smtp.gmail.com", 465, SecureSocketOptions.SslOnConnect);
                await client.AuthenticateAsync(authenticationUserName, authenticationPassword);
                await client.SendAsync(emailMessage);
                await client.DisconnectAsync(true);
            }
        }
    }
}
