﻿using System;
using Lab02_DelegatesAndEvents.Lib.AsyncInvoke;
using Lab02_DelegatesAndEvents.Lib.Clock;

namespace Lab02_DelegatesAndEvents.App
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("1. Clock exercise\n" +
                              "2. Async exercise\n" +
                              "Press selected number.\n");

            char selection = Console.ReadKey().KeyChar;
            Console.Clear();

            switch (selection)
            {
                case '1':
                    ClockExercise();
                    break;
                case '2':
                    AsyncExercise();
                    break;
                default:
                    Console.WriteLine("Wrong selection.");
                    break;
            }
            Console.ReadLine();
        }

        private static void AsyncExercise()
        {
            AsyncInvocation asyncInvocation = new AsyncInvocation();

            asyncInvocation.AddDelegate(FirstMethod);
            asyncInvocation.AddDelegate(SecondMethod);
            asyncInvocation.AddDelegate(ThirdMethod);

            asyncInvocation.InvokeDelegates();
        }

        private static void ClockExercise()
        {
            MyClock myClock = new MyClock();

            Listener firstListener = new Listener("First Listener");
            Listener secondListener = new Listener("Second Listener");
            Listener thirdListener = new Listener("Third Listener");

            myClock.Run();
            firstListener.Subscribe(myClock);
            secondListener.Subscribe(myClock);
            thirdListener.Subscribe(myClock);
        }

        private static int FirstMethod()
        {
            Console.WriteLine("Executing first method");
            return 1;
        }

        private static int SecondMethod()
        {
            Console.WriteLine("Executing second method");
            return 2;
        }

        private static int ThirdMethod()
        {
            Console.WriteLine("Executing third method");
            return 3;
        }
    }
}
