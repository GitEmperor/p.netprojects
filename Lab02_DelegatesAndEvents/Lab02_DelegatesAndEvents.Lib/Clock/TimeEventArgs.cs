using System;

namespace Lab02_DelegatesAndEvents.Lib.Clock
{
    public class TimeEventArgs
    {
        public DateTime ArgsDateTime { get; set; }

        public TimeEventArgs(DateTime argsDateTime)
        {
            ArgsDateTime = argsDateTime;
        }
    }
}