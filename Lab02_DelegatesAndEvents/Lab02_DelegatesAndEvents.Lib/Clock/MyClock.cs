﻿using System;
using System.Threading;
using System.Timers;

namespace Lab02_DelegatesAndEvents.Lib.Clock
{
    public delegate void DelClockNotification(MyClock myClock, TimeEventArgs args);

    public class MyClock
    {
        public DelClockNotification Handler { get; set; }

        private System.Timers.Timer _timer;
        private DateTime _currentTime;

        public MyClock()
        {
            _timer = new System.Timers.Timer(1000);
            _timer.Elapsed += _timer_Elapsed;
        }

        public void Run()
        {
            Thread runThread = new Thread(RunInNewThread);
            runThread.Start();
        }

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Handler?.Invoke(this, new TimeEventArgs(_currentTime));
        }

        private void RunInNewThread()
        {
            while (true)
            {
                _currentTime = DateTime.Now;
                _timer.Enabled = true;
            }
        }
    }
}
