﻿using System;

namespace Lab02_DelegatesAndEvents.Lib.Clock
{
    public class Listener
    {
        public string Name { get; }

        public Listener(string name)
        {
            Name = name;
        }

        public void Subscribe(MyClock myClock)
        {
            myClock.Handler += HandleEvent;
        }

        public void HandleEvent(MyClock myClock, TimeEventArgs args)
        {
            Console.WriteLine("Listener name: {0}, time: {1}", Name, args.ArgsDateTime);
        }
    }
}
