﻿using System;
using System.Runtime.Remoting.Messaging;

namespace Lab02_DelegatesAndEvents.Lib.AsyncInvoke
{
    public delegate int AsyncEventHandler();
     
    public class AsyncInvocation
    {
        public event AsyncEventHandler AsyncEvent;
        private AsyncCallback _asyncCallback;

        public AsyncInvocation()
        {
            _asyncCallback = CallbackMethod;
        }

        public void AddDelegate(AsyncEventHandler newDelegate)
        {
            AsyncEvent += newDelegate;
        }

        public void InvokeDelegates()
        {
            if (AsyncEvent != null)
                foreach (AsyncEventHandler d in AsyncEvent.GetInvocationList())
                {
                    d.BeginInvoke(_asyncCallback, null);
                }
        }

        private void CallbackMethod(IAsyncResult result)
        {
            AsyncResult delegateResult = (AsyncResult)result;
            AsyncEventHandler delegateInstance = (AsyncEventHandler)delegateResult.AsyncDelegate;

            Console.WriteLine("Instance method: {0}, result: {1}", delegateInstance.Method, delegateInstance.EndInvoke(result));
        }
    }
}
